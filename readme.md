# JobSity PHP Challenge repository

This is a main repository that has

- makefile to run common tasks
- all dockers (from laradock) and its configuration
- backend submodule

## Installation

- Clone main repo:
```bash
git clone git@git.jobsity.com:jobsity/php-challenge.git jobsity-codetest

cd jobsity-codetest
```

- Configurations
The actual configuration is ready to run in localhost:1000, but for make it work in, for example, http://php-test.com you have to edit the file [docker/envs/.env.mysql.example](docker/envs/.env.mysql.example)
```
APP_URL=http://php-test.com
```
And you have to edit and follow this line in /etc/hosts file:
```
127.0.0.1   php-test.com
```

- Create project, migrations, seeds, etc.
```bash
make init
```

### Possible errors
- If something fails at this point you should try:
```bash
docker-compose stop
sudo service docker restart
docker-composer up -d
```

- if the database migration or seeds could not finish successfully 
```bash
make migrate-refresh
```

## Urls

- Project API::
http://php-test.com/ - [API](http://php-test.com/)

- MySQL Client:
http://localhost:8078 - [Client](http://localhost:8078)
Server: mysql (docker's name)
Port: 3306
Username: username
Password: password
Database: database

### API`s endpoints
- POST-> http://php-test.com/user
Parameters:
String: email
String: name
String: password

- POST-> http://php-test.com/login
Parameters:
String: email
String: password

Return:
    Json: 
        success, message, token

- GET-> http://php-test.com/stock?q=aapl.us (Needs Authentication tocken)
Return: Json with required data

- GET-> http://php-test.com/history  (Needs Authentication tocken)
Return: Json with required data

## Structure

git submodules

- folder `php-challenge` [project](https://git.jobsity.com/jobsity/php-challenge/-/tree/master) API branch
- folder `docker` has configuration files for `docker`


## users credentials 

| User                         | Password     | Name                     |
| ---------------------------- | ------------ | ------------------------ |
| marianovalderrey@hotmail.com | password1234 | Mariano Valderrey        |
| marianovalderrey@gmail.com   | password4321 | Mariano Valderrey Pechin |


``` nginx container bash console
make bash
```

## 404 Page
This could be because the project is running with other location than localhost:1000 so you have to configure the new name:
Edit .env file:
```
APP_URL=https://php-test.com
```

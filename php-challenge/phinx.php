<?php

return
[
    'paths' => [
        'migrations' => '%%PHINX_CONFIG_DIR%%/database/migrations',
        'seeds' => '%%PHINX_CONFIG_DIR%%/database/seeds'
    ],
    'environments' => [
        'default_migration_table' => 'phinxlog',
        'default_database' => 'app_test',
        'default_environment' => 'app_test',
        'app_test' => [
            'adapter' => 'mysql',
            'host' => env('DB_HOST', 'mysql'),
            'name' => env('DB_DATABASE', 'app_test'),
            'user' => env('DB_USERNAME', 'app_test_user'),
            'pass' => env('DB_PASSWORD', 'app_test@pass'),
            'port' => env('DB_PORT', '3306'),
            'charset' => 'utf8',
        ]
    ],
    //'version_order' => 'creation'
];

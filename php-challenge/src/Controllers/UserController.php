<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Repositories\HistoryRepository;
use App\Repositories\UserRepository;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Respect\Validation\Validator as V;
use Awurth\SlimValidation\Validator;
use Firebase\JWT\JWT;

class UserController 
{
    protected $userRepository;
    protected $historyRepository;
    protected $secret_key;

    /**
     * HelloController constructor.
     */
    public function __construct(UserRepository $userRepository, HistoryRepository $historyRepository){
        $this->historyRepository = $historyRepository;
        $this->userRepository = $userRepository;
        $this->secret_key = env("JWT_PASSWORD_KEY");
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function store(Request $request, Response $response): Response
    {
        $validatorObj = new Validator();
        $validator = $validatorObj->validate($request, [
            "name" => V::length(5, 255),
            "email" => V::email(),
            "password" => V::length(8, 255)
        ]);

        if ($validator->isValid()){
            if ($this->userRepository->findByEmail($validator->getValue("email"))){
                return $response->withStatus(400, "User with this email aready exists");
            }
            $validator->setValue("password", pass_crypt($validator->getValue("password")));
            
            $user = $this->userRepository->create($validator->getValues());
            $user->save();
        }
        else {
             return $response->withStatus(400, "Input does not valdiate.");
        }

        return $response->withStatus(200, "User created just fine!");
    }

    public function history (Request $request, Response $response){
        $token = str_replace("Bearer ", "", $request->getHeaderLine("Authorization"));
        if (!$token){
            return $response->withStatus("401", "Unauthorized access.");
            die();
        }
        
        $decoded_data = JWT::decode($token, $this->secret_key, array_keys(JWT::$supported_algs));
        if (!(time() > $decoded_data->iat && time() < $decoded_data->exp)){
            return $response->withStatus("401", "Unauthorized access.");
            die();
        }
        $user_id = $decoded_data->data->user_id;
        $user = $this->userRepository->findById($user_id, ["history"]);

        $response->getBody()->write(json_encode($user->history->jsonSerialize()));
        $response->withStatus(200);
        return $response->withHeader('Content-Type', 'application/json');
    }

    public function stock(Request $request, Response $response, array $args){
        $token = str_replace("Bearer ", "", $request->getHeaderLine("Authorization"));
        if (!$token){
            return $response->withStatus("401", "Unauthorized access.");
            die();
        }
        
        $decoded_data = JWT::decode($token, $this->secret_key, array_keys(JWT::$supported_algs));
        if (!(time() > $decoded_data->iat && time() < $decoded_data->exp)){
            return $response->withStatus("401", "Unauthorized access.");
            die();
        }
        $q = $request->getQueryParams()["q"];
        $results = $this->getAPIValues($q);
        $names  = explode(",", $results[0]);
        $values = explode(",", $results[1]);
        
        $user_id = $decoded_data->data->user_id;
        $history = $this->historyRepository->createFromAPIValues($user_id, $names, $values);
        
        $response->getBody()->write(json_encode($history->jsonSerialize()));
        $response->withStatus(200);
        return $response->withHeader('Content-Type', 'application/json');
    }

    private static function getAPIValues (string $q){
        $url = "https://stooq.com/q/l/?s=" . $q . "&f=sd2t2ohlcvn&h&e=csv";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_USERPWD, "username:password");

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);
        curl_close($curl);

        return preg_split('/\r\n|\r|\n/', $result);
    }
    public function login(Request $request, Response $response){
        $validatorObj = new Validator();
        $validator = $validatorObj->validate($request, [
            "email" => V::email(),
            "password" => V::length(8, 255)
        ]);
        
        if ($validator->isValid()){
            $user = $this->userRepository->findByEmail($validator->getValue("email"));
            if ($user && pass_crypt_verify($validator->getValue("password"), $user->password)){
                $iat = time();
                // jwt valid for 60 days (60 seconds * 60 minutes * 24 hours * 60 days)
                $exp = $iat + 60 * 60 * 24 * 60;
                $payload = array(
                    'iat' => $iat,
                    'exp' => $exp,
                    "data" => [
                        "user_id" => $user->id
                    ]
                );

                $token = JWT::encode($payload, $this->secret_key);
                $data = [
                    'success' => true,
                    'message' => "Login Successfull",
                    'token' => $token
                ];
                
                $response->getBody()->write(json_encode($data));
                return $response->withHeader('Content-Type', 'application/json');
            }
            else {
                return $response->withStatus(400, "Wrong credentials");
            }
        }
        else {
            return $response->withStatus(400, "Wrong credentials");
        }
    }
}

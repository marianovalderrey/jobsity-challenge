<?php

namespace App\Repositories;

use App\Models\History;
use App\Repositories\Contracts\HistoryRepositoryContract;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UserRepository
 * @package App\Repositories
 */
class HistoryRepository extends BaseRepository implements HistoryRepositoryContract
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id'
    ];
    
    /**
     * include_relations
     *
     * @var array
     */
    protected $include_relations = [
        'user',
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable(): array
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model(): string
    {
        return History::class;
    }

    /**
     * Create model record
     *
     * @param array $input
     *
     * @return Model
     */
    public function create(array $input):Model
    {
        $model = new $this->model();
        $model->fill($input);
        $model->save();

        return $model;
    }
    
    /**
     * findByEmail
     *
     * @param  mixed $email
     * 
     * @return Model
     */
    public function findByUserId(int $user_id): ?Collection
    {
        $query = $this->model->query();
        return $query->where('user_id', $user_id)->get();
    }

    /**
     * Create model record from $columns / $values data from API CALL
     *
     * @param  array $columns
     * @param  array $values
     * 
     * @return Model
     */
    public function createFromAPIValues (int $user_id, array $columns, array $values): Model{
        $model = new $this->model();
        $model->user_id = $user_id;
        $model->fillObjectWithAPIValues($columns, $values);
        $model->save();

        return $model;
    }
}

<?php

namespace App\Repositories\Contracts;

use Illuminate\Database\Eloquent\Model;

interface UserRepositoryContract
{
    public function findByName(string $username): ?Model;
    public function findByEmail(string $email): ?Model;
    public function findFeatured(string $id): Model;
    public function update(array $input, string $id): Model;
    public function create(array $input): Model;
}

<?php

namespace App\Repositories\Contracts;

use Illuminate\Database\Eloquent\Model;

interface BaseRepositoryContract
{
    public function makeModel(): Model;
    public function create(array $input): Model;
    public function getFieldsSearchable();
}

<?php

namespace App\Repositories\Contracts;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface HistoryRepositoryContract
{
    public function findByUserId(int $user_id): ?Collection;
    public function create(array $input): Model;
    public function createFromAPIValues (int $user_id, array $columns, array $values): Model;
}

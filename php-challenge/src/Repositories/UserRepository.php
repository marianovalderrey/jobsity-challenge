<?php

namespace App\Repositories;

use App\Models\User;
use App\Repositories\Contracts\UserRepositoryContract;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UserRepository
 * @package App\Repositories
 */
class UserRepository extends BaseRepository implements UserRepositoryContract
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'email'
    ];
    
    /**
     * include_relations
     *
     * @var array
     */
    protected $include_relations = [
        'history',
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable(): array
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model(): string
    {
        return User::class;
    }

    /**
     * Update model record for given id
     *
     * @param array $input
     * @param string $id
     *
     * @return Model
     */
    public function update(array $input, string $id):Model
    {
        $query = $this->model->newQuery();
        $model = $query->findOrFail($id);
        $model->fill($input);
        $model->save();

        return $model;
    }
    
    /**
     * Create model record
     *
     * @param array $input
     *
     * @return Model
     */
    public function create(array $input):Model
    {
        $model = new $this->model();
        $model->fill($input);
        $model->save();

        return $model;
    }
    
    /**
     * findByEmail
     *
     * @param  mixed $email
     * 
     * @return Model
     */
    public function findByEmail(string $email): ?Model
    {
        $query = $this->model->query();
        return $query->where('email', $email)->first();
    }
    
    /**
     * findByName
     *
     * @param  mixed $name
     * 
     * @return Model
     */
    public function findByName(string $name): ?Model
    {
        $query = $this->model->query();
        return $query->where('name', $name)->first();
    }
    
    /**
     * findById
     *
     * @param  mixed $id
     *
     * @return Model
     */
    public function findById(int $id, array $with = array()): ?Model
    {
        $query = $this->model->query();
        $query->with($with);
        return $query->where('id', $id)->first();
    }
    
    /**
     * findFeatured
     *
     * @param  string $id
     * 
     * @return Model
     */
    public function findFeatured(string $id): User
    {
        $query = $this->model->query();
        $query = $query->with($this->include_relations);
        $query = $query->where("id", $id);
        return $query->first();
    }
    
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Stock
 *
 * @package App\Models
 * @property int $id
 * @property string $name
 * @property string|null $open
 * @property integer $high
 * @property integer $low
 * @property integer $close
 * @property integer $volume
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 */
class History extends Model
{
  use SoftDeletes;
  
  protected $table = "history";
  
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'user_id',
    'name',
    'symbol',
    'open',
    'high',
    'low',
    'close',
    'date',
  ];

  protected $dates = [
    'date',
  ];

  public static $rules = [
    'name' => 'required',
    'symbol' => 'required',
    'open' => 'required',
    'high' => 'required',
    'low' => 'required',
    'close' => 'required',
    'date' => 'required',
  ];

  /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

  public function user()
  {
    return $this->belongsTo(User::class, "user_id");
  }
  
  /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

  /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */
  public function fillObjectWithAPIValues(array $columns, array $values){
      foreach($columns as $index => $column) {
          $column = strtolower($column);
          if ($this->isFillable($column)){
              $this->$column = $values[$index];
          }
      }
  }

  /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

}

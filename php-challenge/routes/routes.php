<?php

declare(strict_types=1);

//use App\Support\Route;
use App\Controllers\HelloController;
use App\Controllers\UserController;
use Slim\App;

return function (App $app) {
    
    $app->post('/user', UserController::class . ":store");
    $app->post('/login', UserController::class . ":login");
    $app->get('/history', UserController::class . ":history");
    $app->get('/stock', UserController::class . ":stock");
    // unprotected routes
    $app->get('/hello/{name}', HelloController::class . ':hello');

    // protected routes
    $app->get('/bye/{name}', HelloController::class . ':bye');
};
/*Route::get("/hello/{name}", 'HelloController@hello');
Route::get("/bye/{name}", 'HelloController@bye');*/

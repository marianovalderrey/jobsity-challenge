<?php

use DI\ContainerBuilder;
use Slim\Factory\AppFactory;
use Symfony\Component\Dotenv\Dotenv;

require __DIR__ . '/../vendor/autoload.php';

//require __DIR__ . '/settings.php';

$dotenv = new Dotenv();
$dotenv->load(__DIR__ . '/../.env');

$ENV = $_ENV['ENV'] ?? 'dev';

$containerBuilder = new ContainerBuilder();

// Set up settings
//$containerBuilder->addDefinitions(__DIR__ . '/container.php');

// Import services
$dependencies = require __DIR__ . '/../app/services.php';
$dependencies($containerBuilder);

// Initialize app with PHP-DI
$container = $containerBuilder->build();
AppFactory::setContainer($container);

$app = AppFactory::create();
$app->addRoutingMiddleware();

// Register routes
$routes = require __DIR__ . '/../routes/routes.php';
$routes($app);

// Setup Basic Auth
$auth = require __DIR__ . '/../routes/auth.php';
$auth($app);

// Eloquent
$eloquent = require __DIR__ . '/eloquent.php';
$eloquent($app);

// Register middleware
//$middleware = require __DIR__ . '/middleware.php';
//$middleware($app);


$displayErrorDetails = $ENV == 'dev';
$errorMiddleware = $app->addErrorMiddleware($displayErrorDetails, true, true);

// Error Handler
$errorHandler = $errorMiddleware->getDefaultErrorHandler();
$errorHandler->forceContentType('application/json');

return $app;

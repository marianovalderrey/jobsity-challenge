<?php 
use Slim\App; 
use Illuminate\Database\Capsule\Manager;

//use App\Utility\Configuration;

return static function (App $app) {

    $container = $app->getContainer();
    $dbSettings = $container->get('settings')['db'];
    
    // boot eloquent
    $capsule = new Manager;
    $capsule->addConnection($dbSettings);
    $capsule->setAsGlobal();
    $capsule->bootEloquent();
  
    return $capsule;
};
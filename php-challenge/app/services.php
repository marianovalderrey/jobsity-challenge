<?php
declare(strict_types=1);

use DI\ContainerBuilder;
use Psr\Container\ContainerInterface;
use Illuminate\Database\Capsule\Manager as Capsule;
use App\Factory\DatabaseManagerFactory;
use Psr\Http\Message\ResponseInterface;

return function (ContainerBuilder $containerBuilder) {
    $containerBuilder->addDefinitions([
        Swift_Mailer::class => function() {
            $host = $_ENV['MAILER_HOST'] ?? 'smtp.mailtrap.io';
            $port = intval($_ENV['MAILER_PORT']) ?? 465;
            $username = $_ENV['MAILER_USERNAME'] ?? 'test';
            $password = $_ENV['MAILER_PASSWORD'] ?? 'test';

            $transport = (new Swift_SmtpTransport($host, $port))
                ->setUsername($username)
                ->setPassword($password)
            ;

            return new Swift_Mailer($transport);
        },
        'settings' => function () {
            return require __DIR__ . '/../config/settings.php';
        },
        Capsule::class => function (ContainerInterface $container) {
            $factory = new DatabaseManagerFactory($container);
            return new $factory->capsule;
        },
        'validator' => function (ContainerInterface $container) {
            return new Awurth\SlimValidation\Validator;
        },
        'loggedInMiddleware' => function ($request, $handler): ResponseInterface {
            
        }
        
    ]);

};

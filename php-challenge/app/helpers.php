<?php

use Illuminate\Support\Collection;


if (!function_exists("base_path")){
    function base_path($path){
        return __DIR__ . "/../{$path}";
    }
}

if (!function_exists("database_path")){
    function database_path($path){
        return base_path("database/{$path}");
    }
}

if (!function_exists("collect")){
    function collect($items){
        return new Collection($items);
    }
}

if (!function_exists("factory")){
    function factory(string $model, int $count = 1){
        $factory = new Factory;

        return $factory ($model, $count);
    }
}

if (!function_exists("throw_when")){
    function throw_when(bool $fails, string $message, string $exception = Exception::class){
        if (!$fails){
            return;
        }

        throw new $exception($message);
    }
}

if (!function_exists("pass_crypt")){
    function pass_crypt(string $password):string {
        return password_hash($password, env("PASSWORD_ENCRYPT"));
    }
}

if (!function_exists("pass_crypt_verify")){
    function pass_crypt_verify(string $password, string $crypt_pass):string {
        return password_verify($password, $crypt_pass);
    }
}
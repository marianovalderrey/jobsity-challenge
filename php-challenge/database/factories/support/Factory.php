<?php 

class Factory {
    public static $definitions;

    public function __invoke(string $model, $count = 1){
        $this->loadFactoryFor($model);

        return FactoryMakeOrCreate::options($model, $count, static::$definitions[$model]);
    }

    public function loadFactoryFor($model){
        $factory = "{$model}Factory";

        require_once ("../../factories/{$factory}.php");
    }

    public static function define (string $model, callable $faker){
        self::$definitions[$model] = $faker;

        return new static;
    }
}
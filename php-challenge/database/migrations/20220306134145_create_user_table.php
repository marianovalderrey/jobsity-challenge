<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CreateUserTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $user = $this->table('user');
        //$user->addColumn('id', 'integer', ['unique' => true, 'null' => false]);
        $user->addColumn('name', 'string', ['null' => false]);
        $user->addColumn('email', 'string', ['null' => false]);
        $user->addColumn('password', 'string', ['null' => false]);
        $user->addColumn('created_at', 'datetime', ['default' => 'CURRENT_TIMESTAMP']);
        $user->addColumn('updated_at', 'datetime', ['default' => 'CURRENT_TIMESTAMP']);
        $user->addColumn('deleted_at', 'datetime', ['null' => true]);
        $user->addIndex(['email'], ['unique' => true]);
        $user->save();
        
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->dropTable('user');
    }

}

<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CreateHistoryTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $history = $this->table('history');
        //$user->addColumn('id', 'integer', ['unique' => true, 'null' => false]);
        $history->addColumn('user_id', 'integer', ['null' => false]);
        $history->addColumn('date', 'datetime', ['null' => false]);
        $history->addColumn('name', 'string', ['null' => false]);
        $history->addColumn('symbol', 'string', ['null' => false]);
        $history->addColumn('open', 'string', ['null' => false]);
        $history->addColumn('high', 'string', ['null' => false]);
        $history->addColumn('low', 'string', ['null' => false]);
        $history->addColumn('close', 'string', ['null' => false]);
        $history->addColumn('created_at', 'datetime', ['default' => 'CURRENT_TIMESTAMP']);
        $history->addColumn('updated_at', 'datetime', ['default' => 'CURRENT_TIMESTAMP']);
        $history->addColumn('deleted_at', 'datetime', ['null' => true]);
        $history->save();
        
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->dropTable('history');
    }
}

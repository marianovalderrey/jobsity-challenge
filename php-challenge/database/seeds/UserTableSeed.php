<?php

use App\Models\User;
use Phinx\Seed\AbstractSeed;

class UserTableSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $users = [[
            'name' => 'Mariano Valderrey',
            'email' => 'marianovalderrey@hotmail.com',
            'password' => pass_crypt('password1234')
        ],[
            'name' => 'Mariano Valderrey Pechin',
            'email' => 'marianovalderrey@gmail.com',
            'password' => pass_crypt('password4321')
        ]];
        
        for ($i = 0; $i < 10; $i++) {
            $users[] = [
                'name' => $faker->name,
                'email' => $faker->unique()->safeEmail,
                'password' => pass_crypt($faker->password)
            ];
        }

        $this->insert("user", $users);
    }
}

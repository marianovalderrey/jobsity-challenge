submodule:
	-echo "Submodules"
	-git submodule init
	-git submodule update

update: 
	-git pull --recurse-submodules
	-git submodule update --recursive --remote
	
envs:
	@echo creating laravel env file from default
	-cp docker/envs/.env.mysql.example php-challenge/.env

docker-up:
	@echo runnig docker
	-docker-compose up -d
bash:
	@echo runnig bash
	-docker-compose exec test_app bash

migrate-refresh:
	@echo runnig migrate-refresh
	-docker-compose exec test_app bash -c "vendor/bin/phinx migrate"
	-docker-compose exec test_app bash -c "vendor/bin/phinx seed:run"


phinx-init:
	@echo runnig phinx-init
	-docker-compose exec test_app bash -c "vendor/bin/phinx init"

clear-cache:
	@echo clear cache
	-docker-compose exec test_app bash -c "php artisan cache:clear"
	-docker-compose exec test_app bash -c "php artisan clear-compiled"
	-docker-compose exec test_app bash -c "php artisan route:clear"
	-docker-compose exec test_app bash -c "php artisan config:clear"
	-docker-compose exec test_app bash -c "php artisan view:clear"

install:
	-docker-compose exec test_app bash -c "composer install require slim/slim:3.*"

composer-update:
	@echo runnig composer update
	-docker-compose exec test_app bash -c "composer update --no-interaction"

composer-dump-autoload:
	@echo runnig composer dump-autoload
	-docker-compose exec test_app bash -c "composer dump-autoload"


init: submodule envs docker-up install composer-update phinx-init migrate-refresh
